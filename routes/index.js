var express = require('express');
var router = express.Router();
let listItem = require('../API/listItem')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/products', function(req, res, next) {
  res.render('products',{listItem:listItem});
  
});
router.get('/dua-hau', function(req, res, next) {
  res.render('duahau');
});
router.get('/login', function(req, res, next) {
  res.render('login');
});
router.post('/profile', function(req, res, next) {
  let {username, password} = req.body
  console.log(req.body)
  res.render('profile',{username})
});

module.exports = router;
